//
//  addTeamViewController.swift
//  CoreDataAssignment
//
//  Created by Moses Olawoyin on 06/11/2019.
//  Copyright © 2019 Moses Olawoyin. All rights reserved.
//

import UIKit

class addTeamViewController: UIViewController {
    
    //Outlets
    
    @IBOutlet var nameTextField: UITextField!
    @IBOutlet var colourTextField: UITextField!
    
    //Variables
    
    
    
    //Constants
    let coreDataController = CoreDataController.shared
    
    
    
    @IBAction func addButtonPressed(_ sender: UIButton) {
        
        
        let mainContext = coreDataController.mainContext
        let newTeam = Team(context: mainContext)
        
        newTeam.teamName = nameTextField.text
        newTeam.teamColor = colourTextField.text
        //        outPutLabel.text = coreDataController.saveContext() ? "Add Team": nil
        nameTextField.text =  coreDataController.saveContext() ? "Saved": nil

        nameTextField.text = nil
        colourTextField.text = nil
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        nameTextField.delegate = self
    }
}

extension addTeamViewController: UITextFieldDelegate{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        //                guard let text = textField.text else {return true}
        //                let moc = coreDataController.shared.mainContext
        //                let team = Team(context: moc)
        //                team.team_name = text
        
        
        //                print(" \(team.team_name)")
        textField.resignFirstResponder()
        return true
    }
}
