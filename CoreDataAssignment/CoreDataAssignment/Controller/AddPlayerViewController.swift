//
//  AddPlayerViewController.swift
//  CoreDataAssignment
//
//  Created by Moses Olawoyin on 07/11/2019.
//  Copyright © 2019 Moses Olawoyin. All rights reserved.
//

import UIKit

class AddPlayerViewController: UIViewController {
    
    
    //Outlets
    @IBOutlet var playerNameTextField: UITextField!
    @IBOutlet var playerAgeTextField: UITextField!
    
    
    
    //Variables
    
    
    //Constants
    
    
    //Actions
    
    @IBAction func addPlayerButton(_ sender: UIButton) {
    }
    
    

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
