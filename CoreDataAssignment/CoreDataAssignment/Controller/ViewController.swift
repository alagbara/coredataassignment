//
//  ViewController.swift
//  CoreDataAssignment
//
//  Created by Moses Olawoyin on 06/11/2019.
//  Copyright © 2019 Moses Olawoyin. All rights reserved.
//

import UIKit
import CoreData

class ViewController: UIViewController, NSFetchedResultsControllerDelegate {
    
    //Outlets
    
    @IBOutlet var teamTableView: UITableView!
    //variables
    
    //Constants
    let coreDataController = CoreDataController.shared
    
    
    let cellid = "cellId"
    
    lazy var fetchedResultsController: NSFetchedResultsController = { () -> NSFetchedResultsController<Team> in
        
        // Create a request to fetch ALL Artists
        let fetchRequest = Team.fetchRequest() as NSFetchRequest<Team>
        
        // Create sort decriptors to sort via age and firstName
        let nameSort = NSSortDescriptor(key: "teamName", ascending: true)
        
        // Add the sort descriptors to the fetch request
        fetchRequest.sortDescriptors = [nameSort]
        
        // Set the batch size
        fetchRequest.fetchBatchSize = 10
        
        let frc = NSFetchedResultsController(fetchRequest: fetchRequest,
                                             managedObjectContext: coreDataController.mainContext,
                                             sectionNameKeyPath: nil,
                                             cacheName: nil)
        
        frc.delegate = self as? NSFetchedResultsControllerDelegate
        
        return frc
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        teamTableView.register(UITableViewCell.self, forCellReuseIdentifier: cellid)
        //    teamTableView.delegate = self
        //        read()
        do {
            try fetchedResultsController.performFetch()
        } catch {
            print(error.localizedDescription)
        }
        print(FileManager.default.urls(for: .documentDirectory, in: .userDomainMask))
    }
    override func viewWillAppear(_ animated: Bool) {
        
        do {
            try fetchedResultsController.performFetch()
        } catch {
            print(error.localizedDescription)
        }
    }
    
}

extension ViewController : UITableViewDataSource,UITableViewDelegate{
    
    func controllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        teamTableView.beginUpdates()
    }
    
    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        teamTableView.endUpdates()
    }
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {
        switch type {
        case .insert:
            teamTableView.insertRows(at: [newIndexPath!], with: .fade)
        case .delete:
            teamTableView.deleteRows(at: [indexPath!], with: .fade)
        case .update:
            teamTableView.reloadRows(at: [indexPath!], with: .fade)
        case .move:
            teamTableView.moveRow(at: indexPath!, to: newIndexPath!)
        }
    }
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange sectionInfo: NSFetchedResultsSectionInfo, atSectionIndex sectionIndex: Int, for type: NSFetchedResultsChangeType) {
        switch type {
        case .insert:
            teamTableView.insertSections(IndexSet(integer: sectionIndex), with: .fade)
        case .delete:
            teamTableView.deleteSections(IndexSet(integer: sectionIndex), with: .fade)
        case .move:
            break
        case .update:
            break
        }
    }
    
    
//    func numberOfSections(in tableView: UITableView) -> Int {
        
//        guard let sections = fetchedResultsController.sections else {
//            fatalError("No sections in fetchedResultsController")
//        }
//
//        return sections.count
  //  }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
//        guard let sections = fetchedResultsController.sections else {
//            fatalError("No sections in fetchedResultsController")
//        }
//        let sectionInfo = sections[section]
//        return sectionInfo.numberOfObjects
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = teamTableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as? TeamTableViewCell
        
        
        let team = fetchedResultsController.object(at: indexPath)
        
        cell?.textLabel?.text = team.teamName
        cell?.detailTextLabel?.text = team.teamColor
        DispatchQueue.main.async {
            self.teamTableView.reloadData()
        }
        return cell ?? <#default value#>
        
    }
}
