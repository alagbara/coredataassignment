//
//  TeamTableViewCell.swift
//  CoreDataAssignment
//
//  Created by Moses Olawoyin on 06/11/2019.
//  Copyright © 2019 Moses Olawoyin. All rights reserved.
//

import UIKit

class TeamTableViewCell: UITableViewCell {
    
    
    //Outlets
    @IBOutlet var nameLabel: UILabel!    
    @IBOutlet var colorLabel: UILabel!
    @IBOutlet var memberCountLabel: UILabel!
    
    //variables
    
    
    
    //Constants
    
//
//    override func awakeFromNib() {
//        super.awakeFromNib()
//        // Initialization code
//    }
//
//    override func setSelected(_ selected: Bool, animated: Bool) {
//        super.setSelected(selected, animated: animated)
//
//        // Configure the view for the selected state
//    }

}
